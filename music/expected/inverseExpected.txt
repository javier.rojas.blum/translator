I'm beggin', beggin' you
So put your loving hand out, baby
I'm beggin', beggin' you
So put your loving hand out

I'm beggin', beggin' you
So put your loving hand out, baby
I'm beggin', beggin' you
So put your loving hand out, darlin'

I'm beggin', beggin' you
Put your loving hand out, baby
I'm beggin', beggin' you
So put your loving hand out, darlin'

I'm fightin' hard to hold my own
Just can't make it all alone
I'm holdin' on, I can't fall back
I'm just a con 'bout to fade to black

Yeah
I'm beggin', beggin' you
So put your loving hand out, baby
I'm beggin', beggin' you
So put your loving hand out, darlin'

But I keep walkin' on, keep openin' doors
Keep hopin' for, now the door is yours
Keep also home, 'cause I don't wanna live in a broken home
Girl, I'm beggin'

You're the wrong way track from the good
I want to paint in the picture tellin' where we could be at
Like a heart in the best way should
You can give it away, you had it
And you took the pay

What we doin'? What we chasin'?
Why the bottom? Why the basement?
Why we got good shit, don't embrace it?
Why the feel for the need to replace me?

An empty shell
I used to be
The shadow of my life was hangin' over me
A broken man
That I don't know
Won't even stand the devil's dance to win my soul

I need you to understand
Tried so hard to be your man
The kind of man you want in the end
Only then can I begin to live again

'Cause I'm beggin', beggin' you, ah
Uh, put your loving hand out, baby
I'm beggin', beggin' you
And put your loving hand out, darlin'

I'm on my knees when I'm beggin'
'Cause I don't wanna lose you
Hey, yeah, ra-ta-ta-ta

So, anytime I bleed, you let me go
Yeah, anytime I feed, you get me know
Anytime I seek, you let me know
But I planted that seed, just let me go

Riding high when I was king
I played it hard and fast, 'cause I had everything
I walked away, but you warned me then
But easy come and easy go
And it would end

I'm beggin', beggin' you
So put your loving hand out, baby
I'm beggin', beggin' you
So put your loving hand out, darlin'

Put your loving hand out, baby
'Cause I'm beggin'