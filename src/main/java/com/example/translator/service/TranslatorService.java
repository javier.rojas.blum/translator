package com.example.translator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class TranslatorService {

    private static Logger LOG = LoggerFactory.getLogger(TranslatorService.class);
    final String STANZA_SEPARATOR = "\n\n";

    /**
     * Exact file copy
     */
    public void copy(String inputFileName, String outputFileName) throws IOException {
        File outputFile = new File(outputFileName);
        InputStream in = new FileInputStream(inputFileName);
        OutputStream out = new FileOutputStream(outputFile);

        StreamUtils.copy(in, out);
    }

    public String readFile(String inputFileName) throws IOException {
        InputStream in = new FileInputStream(inputFileName);
        return StreamUtils.copyToString(in, StandardCharsets.UTF_8);
    }

    public void writeFile(String content, String outputFile) throws IOException {
        OutputStream out = new FileOutputStream(outputFile);
        StreamUtils.copy(content, StandardCharsets.UTF_8, out);
    }

    public void reverse(String inputFileName, String outputFileName) throws IOException {
        String inputString = readFile(inputFileName);

        String inverseString = Arrays.stream(inputString.split(STANZA_SEPARATOR))
                .collect(ArrayList<String>::new,
                        (arrayList, stanza) -> arrayList.add(0, stanza),
                        ArrayList::addAll)
                .stream().collect(Collectors.joining(STANZA_SEPARATOR));

        writeFile(inverseString, outputFileName);
    }

    public String statistics(String inputFileName, String outputFileName) throws IOException {
        String inputString = readFile(inputFileName);
        AtomicInteger stanzaCount = new AtomicInteger(0);
        HashMap<String, Integer> wordCount = new HashMap<>();

        inputString = inputString.replace("'", " ");

        Arrays.stream(inputString.split(STANZA_SEPARATOR)).parallel()
                .forEach(stanza -> {
                    stanzaCount.addAndGet(1);
                    countWords(wordCount, stanza);
                });

        String mostRepeatedWord = Collections.max(wordCount.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();

        LOG.info("Total estrofas: {}", stanzaCount);
        LOG.info("Conteo de palabras: {}", wordCount);
        LOG.info("Palabra que mas se repite: '{}', {} veces.", mostRepeatedWord, wordCount.get(mostRepeatedWord));

        String stats = new StringBuilder()
                .append("Total estrofas: ").append(stanzaCount).append("\n")
                .append("Palabra que mas se repite: ").append(mostRepeatedWord)
                .toString();

        writeFile(stats, outputFileName);

        return mostRepeatedWord;
    }

    private void countWords(HashMap<String, Integer> wordCount, String stanza) {
        String[] splittedString = stanza.split("\\W+"); // Not an alphanumeric character.

        Arrays.stream(splittedString).parallel()
                .forEach(word -> {
                    if (word != null && !word.equals("")) {
                        String upperCaseWord = word.toUpperCase();
                        addWordCount(wordCount, upperCaseWord);
                    }
                });
    }

    /**
     * Sync method because we are doing parallel work
     */
    private synchronized void addWordCount(HashMap<String, Integer> wordCount, String word) {
        int count = wordCount.getOrDefault(word, 0) + 1;
        wordCount.put(word, count);
    }

    public void replace(String target, String replacement, String inputFileName, String outputFileName) throws IOException {
        String inputString = readFile(inputFileName);
        inputString = inputString.replace("'", " ");

        String result = inputString.replaceAll("(?i)" + target, replacement); // Replace ignore case

        LOG.info(result);

        writeFile(result, outputFileName);
    }
}
