package com.example.translator;

import com.example.translator.service.TranslatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TranslatorApplication implements CommandLineRunner {

    private static Logger LOG = LoggerFactory.getLogger(TranslatorApplication.class);

    private final TranslatorService translatorService;

    public TranslatorApplication(TranslatorService translatorService) {
        this.translatorService = translatorService;
    }

    public static void main(String[] args) {
        SpringApplication.run(TranslatorApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if (args.length != 1) {
            System.out.println("==========");
            System.out.println("TRANSLATOR");
            System.out.println("==========");
            System.out.println("java -jar translator.jar Original.txt");
            return;
        }

        String workingDir = System.getProperty("user.dir");
        String originalFileName = workingDir + "/" + args[0];
        String reverseFileName = workingDir + "/output/estrofasEnOrdenInverso.txt";
        String statsFileName = workingDir + "/output/statistics.txt";
        String finalResultFileName = workingDir + "/output/finaloutput.txt";
        String replacement = "you";

        translatorService.reverse(originalFileName, reverseFileName);
        String mostRepeatedWord = translatorService.statistics(reverseFileName, statsFileName);
        translatorService.replace(mostRepeatedWord, replacement, reverseFileName, finalResultFileName);
    }
}
