package com.example.translator.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class TranslatorServiceTest {

    @Value("${originalFileName}")
    String originalFileName;

    @Value("${reverseExpectedFileName}")
    String reverseExpectedFileName;

    @Value("${statsExpectedFileName}")
    String statsExpectedFileName;

    @Value("${finalResultExpectedFileName}")
    String finalResultExpectedFileName;

    @Value("${copyFileName}")
    String copyFileName;

    @Value("${reverseFileName}")
    String reverseFileName;

    @Value("${statsFileName}")
    String statsFileName;

    @Value("${finalResultFileName}")
    String finalResultFileName;

    @Autowired
    TranslatorService translatorService;

    @Test
    void copy() {
        try {
            translatorService.copy(originalFileName, copyFileName);

            File outputFile = new File(copyFileName);
            assertTrue(outputFile.exists());

            String inputFileContent = translatorService.readFile(originalFileName);
            String outputFileContent = translatorService.readFile(copyFileName);
            assertEquals(inputFileContent, outputFileContent);
        } catch (IOException e) {
            fail(e.getMessage(), e);
        }
    }

    @Test
    void getStringFromInputStream() {
        try {
            String inputFileContent = translatorService.readFile(originalFileName);

            assertNotNull(inputFileContent);
        } catch (IOException e) {
            fail(e.getMessage(), e);
        }
    }

    @Test
    void reverseAndStatisticsAndReplace() {
        reverse();

        String mostRepeatedWord = statistics();
        String replacement = "you";

        replace(mostRepeatedWord, replacement);
    }

    void reverse() {
        try {
            translatorService.reverse(originalFileName, reverseFileName);

            File outputFile = new File(reverseFileName);
            assertTrue(outputFile.exists());

            String expectedFileContent = translatorService.readFile(reverseExpectedFileName);
            String outputFileContent = translatorService.readFile(reverseFileName);
            assertEquals(expectedFileContent, outputFileContent);
        } catch (IOException e) {
            fail(e.getMessage(), e);
        }
    }

    String statistics() {
        String mostRepeatedWord = null;

        try {
            mostRepeatedWord = translatorService.statistics(reverseFileName, statsFileName);

            assertNotNull(mostRepeatedWord);

            File outputFile = new File(statsFileName);
            assertTrue(outputFile.exists());

            String expectedFileContent = translatorService.readFile(statsExpectedFileName);
            String outputFileContent = translatorService.readFile(statsFileName);
            assertEquals(expectedFileContent, outputFileContent);
        } catch (IOException e) {
            fail(e.getMessage(), e);
        }

        return mostRepeatedWord;
    }

    void replace(String mostRepeatedWord, String replacement) {
        try {
            translatorService.replace(mostRepeatedWord, replacement, reverseFileName, finalResultFileName);

            File outputFile = new File(finalResultFileName);
            assertTrue(outputFile.exists());

            String expectedFileContent = translatorService.readFile(finalResultExpectedFileName);
            String outputFileContent = translatorService.readFile(finalResultFileName);
            assertEquals(expectedFileContent, outputFileContent);
        } catch (IOException e) {
            fail(e.getMessage(), e);
        }
    }
}