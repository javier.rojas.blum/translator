# translator

## Configuracion

Ajustar los paths en:

```
src/test/resources/application.properties
```

## Test

Ejecutar:

```
mvn test
``` 

## Command Line Run

```
mkdir output
java -jar Translator.jar Original.txt
```